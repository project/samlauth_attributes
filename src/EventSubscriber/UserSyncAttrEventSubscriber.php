<?php

namespace Drupal\samlauth_attributes\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\TypedData\TypedDataManagerInterface;
use Drupal\samlauth\Event\SamlauthEvents;
use Drupal\samlauth\Event\SamlauthUserSyncEvent;
use Egulias\EmailValidator\EmailValidator;
use Psr\Log\LoggerInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber that synchronizes user properties on a user_sync event.
 *
 * This is basic module functionality, partially driven by config options. It's
 * split out into an event subscriber so that the logic is easier to tweak for
 * individual sites. (Set message or not? Completely break off login if an
 * account with the same name is found, or continue with a non-renamed account?
 * etc.)
 */
class UserSyncAttrEventSubscriber implements EventSubscriberInterface {

  /**
   * The EntityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The typed data manager.
   *
   * @var \Drupal\Core\TypedData\TypedDataManagerInterface
   */
  protected $typedDataManager;

  /**
   * The email validator.
   *
   * @var \Egulias\EmailValidator\EmailValidator
   */
  protected $emailValidator;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * A configuration object containing samlauth settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Construct a new SamlauthUserSyncSubscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The EntityTypeManager service.
   * @param \Egulias\EmailValidator\EmailValidator $email_validator
   *   The email validator.
   * @param \Drupal\Core\TypedData\TypedDataManagerInterface $typed_data_manager
   *   The typed data manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, TypedDataManagerInterface $typed_data_manager, EmailValidator $email_validator, LoggerInterface $logger) {
    $this->entityTypeManager = $entity_type_manager;
    $this->emailValidator = $email_validator;
    $this->logger = $logger;
    $this->typedDataManager = $typed_data_manager;
    $this->config = $config_factory->get('samlauth.authentication');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[SamlauthEvents::USER_SYNC][] = ['onUserSync'];
    return $events;
  }

  /**
   * Performs actions to synchronize users with Factory data on login.
   *
   * @param \Drupal\samlauth\Event\SamlauthUserSyncEvent $event
   *   The event.
   */
  public function onUserSync(SamlauthUserSyncEvent $event) {
    $account = $event->getAccount();
    // Synchronize other attributes.
    if ($account->isNew() || $this->config->get('extra_enforce_attributes')) {
      // echo '<pre>';
      $profile_fields = _samlauth_attributes_custom_profile_fields();
      if (!empty($profile_fields)) {
        foreach ($profile_fields as $field_name => $field_config) {
          $enabled = $this->config->get('extra_saml_attr_' . $field_name . '_enabled');
          if ($enabled) {
            $field_value = $this->getAttributeByConfig('extra_saml_attr_' . $field_name . '_attribute', $event);
            if (!empty($field_value)) {
              if ($field_config->getType() == "string") {
                $account->set($field_name, $field_value);
                $event->markAccountChanged();
              }
              else {
                $this->handleTaxonomyTermFields($field_name, $field_value, $field_config, $event);
              }
            }
          }
        }
      }
    }
    // die;
  }

  /**
   * Returns value from a SAML attribute whose name is configured in our module.
   *
   * This is suitable for single-value attributes. (Most values are.)
   *
   * @param string $config_key
   *   A key in the module's configuration, containing the name of a SAML
   *   attribute.
   * @param \Drupal\samlauth\Event\SamlauthUserSyncEvent $event
   *   The event, which holds the attributes from the SAML response.
   *
   * @return mixed|null
   *   The SAML attribute value; NULL if the attribute value was not found.
   */
  public function getAttributeByConfig($config_key, SamlauthUserSyncEvent $event) {
    $attributes = $event->getAttributes();
    $attribute_name = $this->config->get($config_key);
    return $attribute_name && !empty($attributes[$attribute_name][0]) ? $attributes[$attribute_name][0] : NULL;
  }

  /**
   * Handle taxonomy_term fields.
   */

  public function handleTaxonomyTermFields($field_name, $field_value, $field_config, SamlauthUserSyncEvent $event) {

    $field_setting = $field_config->getSettings();
    $taxo_vid = current(array_keys($field_setting['handler_settings']['target_bundles']));
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($taxo_vid);

    $terms_mapping = [];
    foreach ($terms as $term) {
      $terms_mapping[$term->name] = intval($term->tid);
    }

    $ud_mapping = $this->config->get('extra_saml_attr_' . $field_name . '_mapping');
    $mapped_taxos = $this->getTaxonomyMapping($ud_mapping, $terms_mapping);
    $taxos_to_add = [];
    $field_value = explode(',', $field_value);
    foreach ($field_value as $value) {
      if (isset($mapped_taxos[$value])) {
        foreach ($mapped_taxos[$value] as $tid) {
          $taxos_to_add[] = $tid;
        }
      }
    }

    // Add Default value.
    $default_to_add = $this->getTaxoFieldDefaultValue($field_config);
    if (!empty($default_to_add)) {
      $taxos_to_add = array_merge($taxos_to_add, $default_to_add);
    }

    if (!empty($taxos_to_add)) {
      $account = $event->getAccount();
      $taxos_to_add = array_unique($taxos_to_add);
      $account->set($field_name, $taxos_to_add);
      $event->markAccountChanged();
    }

  }

  /**
   * Fer Taxonomy Field default values.
   */

  public function getTaxoFieldDefaultValue($field_config) {
    $ids = (object) [
      'entity_type' => 'user',
      'bundle' => 'user',
      'entity_id' => NULL,
    ];
    $entity = _field_create_entity_from_ids($ids);
    return array_column($field_config->getDefaultValue($entity), 'target_id');
  }

  /**
   * Get taxonomy mapping.
   */
  public function getTaxonomyMapping($ud_mapping, $taxo_terms) {

    if (empty($ud_mapping)) {
      return $taxo_terms;
    }

    $ud_mapping = explode(PHP_EOL, trim($ud_mapping));
    $mapped_attr = [];
    foreach ($ud_mapping as $line) {
      list($saml_attr, $taxo_name) = explode('|', $line);
      if ($taxo_terms[trim($taxo_name)]) {
        $mapped_attr[trim($saml_attr)][] = $taxo_terms[trim($taxo_name)];
      }
    }
    return $mapped_attr;
  }

}
